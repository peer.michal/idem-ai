import logging
import threading
from functools import lru_cache


def post_setup(hub, ctx):
    """A post_setup contract ensures that a log translation handler can be added to any of the other loggers."""
    if "idem" not in hub.OPT:
        return ctx.ret

    # The user opted out of translated logs in config
    if not hub.OPT.idem.translate_logs:
        return ctx.ret

    # Get the root logger
    root = logging.getLogger()

    if not (hub.OPT.pop_ml.dest_lang or hub.OPT.pop_ml.model_name):
        root.warning(
            "Either a dest_lang or model_name needs to be set in the config file. "
            "I.E. \npop_ml:\n  dest_lang: es"
        )
        return ctx.ret

    # Call the idempotent "init" of pop-ml
    hub.ml.tokenizer.init(
        model_name=hub.OPT.pop_ml.model_name,
        dest_lang=hub.OPT.pop_ml.dest_lang,
        source_lang=hub.OPT.pop_ml.source_lang,
        pretrained_model=hub.OPT.pop_ml.pretrained_model_class,
        pretrained_tokenizer=hub.OPT.pop_ml.pretrained_tokenizer_class,
    )

    class PopMLFormatter(logging.Formatter):
        def __init__(self, original_formatter):
            super().__init__()
            self.original_formatter = original_formatter
            self.translating = threading.RLock()
            # Now our translate method is a cached version of hub.ml.tokenizer.translate
            self.translate = lru_cache(maxsize=hub.OPT.idem.translation_cache_size)(
                hub.ml.tokenizer.translate
            )

        def format(self, record):
            # Prevent infinite recursion by not translating the "tokenizer.translate" lines
            with self.translating:
                # First, format the record with the original formatter
                translation = self.translate(tuple(record.msg.split("\n")))
                record.msg = "\n".join(translation)
                # Then, format the record with the original formatter
                return self.original_formatter.format(record)

    for handler in list(root.handlers):
        # Create a PopMLFormatter with the original handler's formatter
        pop_ml_formatter = PopMLFormatter(handler.formatter)
        # Set the handler's formatter to the PopMLFormatter
        handler.setFormatter(pop_ml_formatter)

    return ctx.ret
