GROUP = "POP Translate"
# https://pop.readthedocs.io/en/latest/tutorial/quickstart.html#adding-configuration-data
CONFIG = {
    "translate_docs": {
        "default": False,
        "help": "Enable the translation of docstrings",
        "dyne": "idem",
    },
    "translate_output": {
        "default": False,
        "help": "Enable the translation of rend output, this may cause unpredictable behavior for CLI programs",
        "dyne": "idem",
    },
    "translate_comments": {
        "default": False,
        "help": "Enable the translation of comment blocks from state and exec modules",
        "dyne": "idem",
    },
    "translate_logs": {
        "default": False,
        "help": "Enable the translation of logs with pop-ml",
        "dyne": "idem",
    },
    "translation_cache_size": {
        "default": 100,
        "help": "The size of the LRU cache that stores frequent translations for the logger",
        "dyne": "idem",
    },
}

SUBCOMMANDS = {}

CLI_CONFIG = {
    "translate_docs": {
        "subcommands": ["doc"],
        "dyne": "idem",
        "action": "store_true",
        "group": GROUP,
    },
    "translate_output": {
        "subcommands": ["_global_"],
        "dyne": "idem",
        "action": "store_true",
        "group": GROUP,
    },
    "translate_comments": {
        "subcommands": ["state", "exec"],
        "dyne": "idem",
        "action": "store_true",
        "group": GROUP,
    },
    "translate_logs": {
        "subcommands": ["_global_"],
        "dyne": "idem",
        "action": "store_true",
        "group": GROUP,
    },
    "translation_cache_size": {
        "subcommands": ["_global_"],
        "dyne": "idem",
        "action": "store_true",
        "group": GROUP,
    },
}

DYNE = {
    "config": ["config"],
    "exec": ["exec"],
    "log": ["log"],
    "output": ["output"],
    "states": ["states"],
    "token": ["token"],
}
