def post(hub, ctx):
    """
    Translate the comments from exec output
    """
    # If there is no "comment" in the output then return right away (i.e. for describe and is_pending)
    if "comment" not in ctx.ret:
        return ctx.ret

    if not hub.OPT.idem.translate_comments:
        return ctx.ret

    if not (hub.OPT.pop_ml.dest_lang or hub.OPT.pop_ml.model_name):
        hub.log.warning(
            "Either a dest_lang or model_name needs to be set in the config file. "
            "I.E. \npop_ml:\n  dest_lang: es"
        )
        return ctx.ret

    # Call the idempotent "init" of pop-ml
    hub.ml.tokenizer.init(
        model_name=hub.OPT.pop_ml.model_name,
        dest_lang=hub.OPT.pop_ml.dest_lang,
        source_lang=hub.OPT.pop_ml.source_lang,
        pretrained_model=hub.OPT.pop_ml.pretrained_model_class,
        pretrained_tokenizer=hub.OPT.pop_ml.pretrained_tokenizer_class,
    )

    comments = ctx.ret["comment"]
    is_string = False
    if isinstance(comments, str):
        is_string = True
        comments = comments.split("\n")

    # Translate the output data
    translated = hub.ml.tokenizer.translate(comments)

    # Replace the comments from the ret with the translated comments
    if is_string:
        ctx.ret["comment"] = "\n".join(translated)
    else:
        ctx.ret["comment"] = translated

    return ctx.ret
