from typing import List


def translate(
    hub,
    ctx,
    *text: List[str],
    dest_lang: str = None,
    source_lang: str = None,
    model_name: str = None,
    pretrained_model: str = None,
    pretrained_tokenizer: str = None,
):
    """An exec module to take input text and translate to the destination language."""
    result = {
        "result": True,
        "comment": [],
        "ret": "",
    }
    if not (
        (dest_lang and source_lang)
        or model_name
        or (pretrained_model and pretrained_tokenizer)
    ):
        result["comment"].append("Using global model")
        # Call the idempotent "init" of pop-ml
        hub.ml.tokenizer.init(
            model_name=model_name or hub.OPT.pop_ml.model_name,
            dest_lang=dest_lang or hub.OPT.pop_ml.dest_lang,
            source_lang=source_lang or hub.OPT.pop_ml.source_lang,
            pretrained_model=pretrained_model or hub.OPT.pop_ml.pretrained_model_class,
            pretrained_tokenizer=pretrained_tokenizer
            or hub.OPT.pop_ml.pretrained_tokenizer_class,
        )
        model = hub.ml.MODEL
        tokenizer = hub.ml.TOKENIZER
    else:
        result["comment"].append("Retrieving specific model")
        # If a specific options were given then get a unique model
        ret = hub.ml.tokenizer.get(
            model_name=model_name or hub.OPT.pop_ml.model_name,
            dest_lang=dest_lang or hub.OPT.pop_ml.dest_lang,
            source_lang=source_lang or hub.OPT.pop_ml.source_lang,
            pretrained_model=pretrained_model or hub.OPT.pop_ml.pretrained_model_class,
            pretrained_tokenizer=pretrained_tokenizer
            or hub.OPT.pop_ml.pretrained_tokenizer_class,
        )
        model = ret.model
        tokenizer = ret.tokenizer

    result["comment"].append(f"Using pretrained model: {model}")
    result["comment"].append(f"Using pretrained tokenizer: {tokenizer}")

    # Translate the text using pop-ml
    result["ret"] = hub.ml.tokenizer.translate(text, model=model, tokenizer=tokenizer)

    return result
