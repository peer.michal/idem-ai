"""
Tests for contracts in idem_ai
"""
import tempfile

import yaml

CONFIG = {
    "idem": {
        "translate_output": False,
        "translate_comments": False,
        "translate_logs": False,
        "translation_cache_size": 50,
    },
    "pop_ml": {
        "dest_lang": "es",
        "source_lang": "en",
    },
}


def test_translate_exec(idem_cli):
    """Verify that the exec module for translation functions properly with explicit parameters."""
    ret = idem_cli("exec", "ml.translate", "Hello World!", "dest_lang=es")
    assert ret.result, ret.stderr
    assert ret.json.ret == ["¡Hola Mundo!"]


def test_translate_exec_with_config(idem_cli):
    """Verify that the exec module for translation functions properly with config values."""
    with tempfile.NamedTemporaryFile(suffix=".cfg", delete=True) as cfg:
        CONFIG["pop_ml"]["dest_lang"] = "es"
        cfg.write(yaml.safe_dump(CONFIG).encode())
        cfg.flush()
        ret = idem_cli("exec", f"--config={cfg.name}", "ml.translate", "Hello World!")
    assert ret.result, ret.stderr
    assert ret.json.ret == ["¡Hola Mundo!"]


def test_translate_state_comment(idem_cli):
    """Verify that a state's comments are translated."""
    with tempfile.NamedTemporaryFile(suffix=".cfg", delete=True) as cfg:
        CONFIG["idem"]["translate_comments"] = True
        cfg.write(yaml.safe_dump(CONFIG).encode())
        cfg.flush()
        ret = idem_cli(
            "exec", f"--config={cfg.name}", "state.run", "time.sleep", "duration=0"
        )

    assert ret.result, ret.stderr
    assert ret.json.comment == ["Durmió con éxito durante 0 segundos."]


def test_translate_exec_comment(idem_cli):
    """
    Verify that a exec module's comments are translated
    """
    with tempfile.NamedTemporaryFile(suffix=".cfg", delete=True) as cfg:
        CONFIG["idem"]["translate_comments"] = True
        cfg.write(yaml.safe_dump(CONFIG).encode())
        cfg.flush()
        ret = idem_cli(
            "exec", f"--config={cfg.name}", "state.run", "time.sleep", "duration=0"
        )

    assert ret.result, ret.stderr
    assert ret.json.comment == ["Durmió con éxito durante 0 segundos."]


def test_translate_logs(idem_cli):
    """Verify that log output is translated."""
    with tempfile.NamedTemporaryFile(suffix=".cfg", delete=True) as cfg:
        CONFIG["idem"]["translate_logs"] = True
        cfg.write(yaml.safe_dump(CONFIG).encode())
        cfg.flush()
        ret = idem_cli("exec", f"--config={cfg.name}", "test.ping", "--log-level=debug")

    assert ret.result, ret.stderr
    assert "[DEBUG   ] Iniciar" in ret.stderr


def test_translate_output(idem_cli):
    """Verify that full output is translated."""
    with tempfile.NamedTemporaryFile(suffix=".cfg", delete=True) as cfg:
        CONFIG["idem"]["translate_output"] = True
        cfg.write(yaml.safe_dump(CONFIG).encode())
        cfg.flush()
        ret = idem_cli("doc", f"--config={cfg.name}", "exec.test.ping", "--output=yaml")

    assert ret.result, ret.stderr
    assert (
        "éxito" in ret.stdout
    ), "Output did not maintain formatting and could not return to json"


def test_translate_docs(idem_cli):
    """
    Verify that "idem doc" doesn't translate everything, just docstrings
    """
    with tempfile.NamedTemporaryFile(suffix=".cfg", delete=True) as cfg:
        CONFIG["idem"]["translate_docs"] = True
        cfg.write(yaml.safe_dump(CONFIG).encode())
        cfg.flush()
        ret = idem_cli(
            "doc",
            f"--config={cfg.name}",
            "exec.test.ping",
            "--translate-docs",
            "--output=json",
            "--log-level=debug",
        )

    assert ret.result, ret.stderr
    assert "exec.test.ping" in ret.json, ret.stderr
    assert "éxito" in ret.json["exec.test.ping"]["doc"]
